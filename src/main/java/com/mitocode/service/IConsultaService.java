package com.mitocode.service;

import java.util.List;

import com.mitocode.model.Consulta;
import com.mitocode.util.ConsultaListaExamen;

public interface IConsultaService {

	Consulta registrar(ConsultaListaExamen consulta);

	void modificar(Consulta consulta);

	void eliminar(int idConsulta);

	Consulta listarId(int idConsulta);

	List<Consulta> listar();
}
